<?php

	/*
		Not part of Core class. Use like this: message($common['common']['age'], 20);
	*/
	function message($text, ...$params) {
		$num_args = func_num_args();
		$args     = func_get_args();
		array_shift($args);

		for($i=0; $i<$num_args-1; $i++) {
			$patern = '{{%(\w+)%}}';
			$text = preg_replace($patern, $params[$i],$text, 1);
		}
		return $text;		
	}
	/*
		The core functionality that is desired to be inheritance by all classes.
		Notice:
			Add functions that are needed in more than one class.
			Take into account inheritence.
	*/
	class Core {


		public function __construct() {}

		/*
			Flats an array.
			Params:
				(ARRAY) $array: an array that we want to make flat.
			Returns:
				(ARRAY) A flatten array.
		*/
		// TODO make it global function!
		protected function flatten_array($array) {			
			$object_temp = (object) array('flatten_array' => array());
			array_walk_recursive($array, create_function('&$v, $k, &$t', '$t->flatten_array[] = $v;'), $object_temp);
			return $object_temp->flatten_array;
		}

		public function objectToArray($object) {
	        if (is_object($object)) {
	            // Gets the properties of the given object
	            // with get_object_vars function
	            $object = get_object_vars($object);
	        }
			
	        if (is_array($object)) {
	            /*
	            * Return array converted to object
	            * Using __FUNCTION__ (Magic constant)
	            * for recursive call
	            */
	            return array_map(__FUNCTION__, $object);
	        }
	        else {
	            // Return array
	            return $object;
	        }
	    }

	    public function current_locale(){
	    	if (isset($_SESSION['language']) && !empty($_SESSION['language'])){
	    		return $_SESSION['language'];
	    	}
	    	else return '';
	    }
	    public function language_switcher($language = ""){
	    	$languages = array('en','el');

	    	if(in_array($language, $languages)) return $_SESSION['language'] = $language;
	    	else return $_SESSION['language'] = $languages[0];
	    }

	    public function url_params() {
	    	if (!isset($_SERVER['QUERY_STRING']) || empty($_SERVER['QUERY_STRING'])) {
	    		return 'language=';
	    	}
	    	return substr($_SERVER['QUERY_STRING'], 0, -2);

 	    }

	    		    /*
	    	Removes the last comma from a given string.
	    	Params:
	    		(STRING) $string: a string
	    	Returns:
	    		(STRING) A string after removing the last comma.
	    */
		public function remove_last_comma($string) {
			// Find the last comma in the string.
			$position = strripos($string, ',');
			// Remove the substring after the last comma.
			$string   = substr($string, 0, $position);
			return $string;
		}


	}



?>