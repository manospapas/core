<?php
	
	$text['common']['language']  = 'Language';
	$text['common']['greek']     = 'Greek';
	$text['common']['english']   = 'English';
	$text['common']['cancel']    = 'Ακύρωση';	
	$text['common']['add']       = 'Προσθήκη';              
	$text['common']['edit']      = 'Επεξεργασία';
	$text['common']['delete']    = 'Διαγραφή';	
	$text['common']['action']    = 'Ενέργειες';
	$text['common']['submit']    = 'Υποβολή';
	$text['common']['reset']     = 'Επαναφορά';
	$text['common']['save']      = 'Αποθήκευση';
	$text['common']['load']      = 'Φόρτωση';
	$text['common']['id']        = 'ID';
	$text['common']['username']  = 'Ψευδώνυμο';
	$text['common']['firstname'] = 'Όνομα';
	$text['common']['lastname']  = 'Επίθετο';
	$text['common']['email']     = 'Εmail';
	$text['common']['role']      = 'Ρόλος';
	$text['common']['password']  = 'Κωδικός';
	$text['common']['choose']    = 'Κανένας';

?>