<?php

	$text['common']['language'] = 'Language';
	$text['common']['greek']    = 'Greek';
	$text['common']['english']  = 'English';
	$text['common']['cancel']   = 'Cancel';	
	$text['common']['add']      = 'Add';        
	$text['common']['edit']     = 'Edit';
	$text['common']['delete']   = 'Delete';	
	$text['common']['action']   = 'Actions';
	$text['common']['submit']   = 'Submit';
	$text['common']['reset']    = 'Reset';
	$text['common']['save']     = 'Save';
	$text['common']['load']     = 'Load';
	$text['common']['id']        = 'ID';
	$text['common']['username']  = 'Username';
	$text['common']['firstname'] = 'Firstname';
	$text['common']['lastname']  = 'Lastname';
	$text['common']['email']     = 'Email';
	$text['common']['role']      = 'Role';
	$text['common']['password']  = 'Password';
	$text['common']['choose']    = 'None';
	
?>