<?php

	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/share/el/common.xml.php';

	// Admin
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/el/header/navbar.xml.php';
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/el/footer/footer.xml.php';
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/el/header/topbar.xml.php';

	//Role Rights
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/el/role_rights/modal.xml.php';
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/el/role_rights/module.xml.php';
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/el/role_rights/rights.xml.php';
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/el/role_rights/role.xml.php';

?>