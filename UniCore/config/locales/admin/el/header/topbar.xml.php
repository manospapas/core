<?php

	$text['topbar']['header']  = 'Θέλετε να αποχωρήσετε;';
	$text['topbar']['message'] = 'Επιλέξτε "Αποσύνδεση" εάν θέλετε να αποχωρήσετε.';
	$text['topbar']['logout']  = 'Αποσύνδεση';
	$text['topbar']['profile']  = 'Profile';
	$text['topbar']['search']  = 'Αναζήτηση';

?>