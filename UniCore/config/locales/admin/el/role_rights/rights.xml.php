<?php

	$text['rights']['access']      = 'Πρόσβαση';
	$text['rights']['view']        = 'Απεικόνιση';
	$text['rights']['row_access']  = 'Γραμμή';
	$text['rights']['full_access'] = 'Πλήρης δικαιώματα';
	$text['rights']['no_access']   = 'Καθόλου δικαιώματα';

?>