<?php


	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/share/en/common.xml.php';

	// Admin
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/en/header/navbar.xml.php';
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/en/footer/footer.xml.php';
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/en/header/topbar.xml.php';

	//Role Rights
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/en/role_rights/modal.xml.php';
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/en/role_rights/module.xml.php';
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/en/role_rights/rights.xml.php';
	include $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/en/role_rights/role.xml.php';


?>