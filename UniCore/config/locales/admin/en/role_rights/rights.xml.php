<?php
	
	$text['rights']['access']      = 'Access';
	$text['rights']['view']        = 'View';
	$text['rights']['row_access']  = 'Row Access';
	$text['rights']['full_access'] = 'Full Access';
	$text['rights']['no_access']   = 'No Access';

?>