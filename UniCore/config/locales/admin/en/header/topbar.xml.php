<?php


	$text['topbar']['header'] = 'Ready to leave?';
	$text['topbar']['message'] = 'Select "Logout" below if you are ready to end your current session';;
	$text['topbar']['logout'] = 'Logout';
	$text['topbar']['profile']  = 'Profile';
	$text['topbar']['search']  = 'Search';

?>