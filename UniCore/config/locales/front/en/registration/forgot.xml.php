<?php
	
	$text['forgot']['header'] = 'Remind';
	$text['forgot']['email'] = 'Email';
	$text['forgot']['placeholder_email'] = 'Email';
	$text['forgot']['reset_password'] = 'Send';
	$text['forgot']['login_page'] = 'Login';
	$text['forgot']['message'] = 'Did you forgot your password? Enter your email and instructions will be sent to your email.';

	//Text for the method reset() at the Registration Class
	$text['forgot']['check_email'] = 'Please check your email for a confirmation link to complete your password reset! Information have been sent to: ';
	$text['forgot']['subject'] = 'Password Reset Link ( mycompany.com )';
	// TODO change the link! Make it dynamic
	$text['forgot']['body'] = 'Hello! You have requested password reset!
			Please click this link to reset your password:
			http://unicore.test/app/views/front/registration/reset.php';
	$text['forgot']['exist'] = 'User with that email doesn\'t exist!';
	$text['forgot']['provide_email'] = 'Please give an email.';
	$text['forgot']['email_send'] = 'An email has been sent to you! Please check your inbox.';
?>