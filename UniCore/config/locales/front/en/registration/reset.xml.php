<?php

	$text['reset']['header'] = 'Reset';
	$text['reset']['password'] ='Password';
 	$text['reset']['repeat_password'] ='Repeat Password';
	$text['reset']['submit'] ='Submit';
	$text['reset']['login'] ='Login';
	$text['reset']['message'] = 'Please enter the password you want.';
	//Text for the method reset() at the Registration Class 
	$text['reset']['success'] = 'The password has been changed successfully!';
	$text['reset']['match'] ='The passwords did not match!';
	$text['reset']['not_found'] = 'The user was not found!';
	$text['reset']['credentials'] = 'Credentials are not correct!';
?>