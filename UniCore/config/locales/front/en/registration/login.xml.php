<?php
	
	$text['login']['header'] = 'Login';
	$text['login']['email'] = 'Email';
	$text['login']['placeholder_email'] = 'Email';
	$text['login']['password'] = 'Password';
	$text['login']['placeholder_password'] = 'Password';
	$text['login']['remember_password'] = 'Remember me';
	$text['login']['forgot_password'] = 'Forgot password';
	$text['login']['login'] = 'Login';

	//Text for the method login() at the Registration Class.
	$text['login']['empty'] = 'User name and/or password must not be empty.';
	$text['login']['exist'] = 'User with that email or password doesn\'t exist!';

?>