<?php
	
	$text['login']['header'] = 'Σύνδεση';
	$text['login']['email'] = 'Email';
	$text['login']['placeholder_email'] = 'Email';
	$text['login']['password'] = 'Κωδικός';
	$text['login']['placeholder_password'] = 'Κωδικός';
	$text['login']['remember_password'] = 'Να θυμάμαι τον κωδικό';
	$text['login']['login'] = 'Σύνδεση';
	$text['login']['forgot_password'] = 'Υπενθύμιση κωδικού';

	//Text for the method login() at the Registration Class.
	$text['login']['empty'] = 'To email ή/και ο κωδικός δεν πρέπει να είναι κενά.';
	$text['login']['exist'] = 'Ο συνδυασμός email και κωδικού δεν είναι σωστά.';


?>