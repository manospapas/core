<?php

	$text['reset']['header'] = 'Επαναφορά';
	$text['reset']['password'] ='Κωδικός';
 	$text['reset']['repeat_password'] ='Επανάληψη Κωδικού';
	$text['reset']['submit'] ='Υποβολή';
	$text['reset']['login'] ='Σύνδεση';
	$text['reset']['message'] = 'Παρακαλώ συμπληρώστε τον κωδικό που θέλετε.';
	//Text for the method reset() at the Registration Class 
	$text['reset']['success'] = 'Ο κωδικός έχει αλλάξει με επιτυχία!';
	$text['reset']['match'] ='Ο κωδικός δεν ήταν ο ίδιος!';
	$text['reset']['not_found'] = 'Δεν βρέθηκε ο χρήστης!';
	$text['reset']['credentials'] = 'Τα στοιχεία που δώθηκαν δεν είναι έγκυρα!';
?>