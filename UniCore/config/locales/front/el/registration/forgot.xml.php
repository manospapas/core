<?php

	$text['forgot']['header'] = 'Υπενθύμιση';
	$text['forgot']['email'] = 'Email';
	$text['forgot']['placeholder_email'] = 'Email';
	$text['forgot']['reset_password'] = 'Αποστολή';
	$text['forgot']['login_page'] = 'Σύνδεση';
	$text['forgot']['message'] = 'Ξεχάσατε τον κωδικό σας? Συμπληρώστε το email σας και θα σας σταλεί email με οδηγίες.';

	//Text for the method forgot() at the Registration Class
	$text['forgot']['check_email'] = 'Παρακαλώ δείτε το email σας. Σας έχουν αποσταλεί οδηγίες στο: ';
	$text['forgot']['subject'] = 'Επαναφορά κωδικού ( mycompany.com )';
	// TODO change the link! Make it dynamic
	$text['forgot']['body'] = 'Χαίρετε! Έχετε ζητήσει επαναφορά κωδικού. Πατήστε στον παρακάτω σύνδεσμο: http://unicore.test/app/views/front/registration/reset.php';
	$text['forgot']['exist'] = 'Δεν υπάρχει χρήστης με αυτό το email.';
	$text['forgot']['provide_email'] = 'Παρακαλώ δώστε ένα email.';
	$text['forgot']['email_send'] = 'Σας έχει αποσταλεί email. Παρακαλώ δείτε τα εισερχόμενα σας.';
?>