<?php
	
	//Credentials
	if (true || filter_input(INPUT_SERVER, 'SERVER_NAME', FILTER_SANITIZE_URL) == "localhost") {
		//Development
		$driver      = 'mysql';
		$db_name     = 'unicore';
		$host        = 'localhost';
		$username    = 'root';
		$db_password = '';
		$charset     = 'utf8mb4';		
	}
	else {
		//Production
		$driver   = '';
		$db_name  = '';
		$host     = '';
		$username = '';
		$password = '';
		$charset  = '';	
	}

?>