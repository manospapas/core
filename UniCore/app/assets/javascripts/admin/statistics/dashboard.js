$(document).ready(function(){
  google.charts.load('current', {'packages':['corechart']});
});

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart(high_income, low_income, aggregates, lower_middle_income, upper_middle_income) {

  // Create the data table.
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Topping');
  data.addColumn('number', 'Slices');
  data.addRows([
    ['High income', high_income],
    ['Low income', low_income],
    ['Aggregates', aggregates],
    ['Lower middle income', lower_middle_income],
    ['Upper middle income', upper_middle_income]
  ]);

  // Set chart options
  var options = {'title':'Income',
                 'width':400,
                 'height':300};

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
  chart.draw(data, options);
}

function renderHTML(data) {
  var htmlString = "";

  high_income = 0;

  low_income = 0;

  aggregates = 0;

  lower_middle_income = 0;

  upper_middle_income = 0;

  htmlString = "<div class='table-responsive'><table class='table table-striped' id='world-bank'>"+
  "<thead class='thead-inverse table-dark'><tr><th>ID</th><th>Income</th></tr></thead><tbody>";

  for (i = 0; i < data[1].length; i++) {
    htmlString += "<tr><td class='align-middle'>" + data[1][i].id + '</td>' 

    htmlString += "<td class='align-middle'>" + data[1][i]['incomeLevel'].value + '</td>'

    htmlString += "</tr>"

    if(data[1][i]['incomeLevel'].value == 'High income'){
      high_income += 1;
    }
    else if(data[1][i]['incomeLevel'].value == 'Low income'){
      low_income += 1;
    }
    else if(data[1][i]['incomeLevel'].value == 'Aggregates'){
      aggregates += 1;
    }
    else if(data[1][i]['incomeLevel'].value == 'Lower middle income'){
      lower_middle_income += 1;
    }
    else {
      upper_middle_income += 1;
    }
  }

  htmlString +="</tbody></table>"

  document.getElementById("world-bank-info").innerHTML = htmlString;

  // Set a callback to run when the Google Visualization API is loaded.
  google.charts.setOnLoadCallback(drawChart(high_income, low_income, aggregates, lower_middle_income, upper_middle_income));
}

var bankContainer = document.getElementById("world-bank-info");

var btn = document.getElementById("btn");

btn.addEventListener("click", function() {
  var ourRequest = new XMLHttpRequest();

  ourRequest.open('GET', 'http://api.worldbank.org/v2/country/all/?format=json');

  ourRequest.onload = function() {
    if (ourRequest.status >= 200 && ourRequest.status < 400) {
      var ourData = JSON.parse(ourRequest.responseText);
      renderHTML(ourData);
    } 
    else {
      console.log("We connected to the server, but it returned an error.");
    }
    
  };

  ourRequest.onerror = function() {
    console.log("Connection error");
  };

  ourRequest.send();
});