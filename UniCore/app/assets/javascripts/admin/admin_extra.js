//Remove elements according to rights
	$(document).ready(function(){
		var elements = document.querySelectorAll(".no-edit");

		if (elements.length > 0) {

			for(i=0; i<500; i++){
				j = i +1;
				disabled_values(j, true);
				document.getElementById("row_access-"+j).disabled = true;
			}

		}

	});

	function select_row(row) {
		if(document.getElementById("row_access-"+row).checked){
			checked_values(row, true);
			disabled_values(row, true);
		}
		else{
			checked_values(row, false);
			disabled_values(row, false);
		}
	}

	function select_all(length, boolean) {
		for(i=0; i<length; i++){
			checked_values((i+1), boolean);
			disabled_values((i+1), boolean);
		}
	}

	function checked_values(row, boolean) {
		document.getElementById("add-"+row).checked = boolean;
		document.getElementById("edit-"+row).checked = boolean;
		document.getElementById("delete-"+row).checked = boolean;
		document.getElementById("view-"+row).checked = boolean;
		document.getElementById("row_access-"+row).checked = boolean;
	}

	function disabled_values(row, boolean) {
		document.getElementById("add-"+row).disabled = boolean;
		document.getElementById("edit-"+row).disabled = boolean;
		document.getElementById("delete-"+row).disabled = boolean;
		document.getElementById("view-"+row).disabled = boolean;
	}

	function reset_form(length) {
		select_all(length, false);
	}


	function view_enabled(value, action,id ) {
		
		if ($('#'+action+id).is(':checked') && document.getElementById(action+id).checked) {

			document.getElementById('view-'+id).checked = true;

		} 
	}

//END OF Remove elements according to rights

$(document).ready(function() {

	var locale = get_current_locale();

	//Module Table
    $('#module-table').DataTable( {
    	"dom": 'l<"inner_search"f>pt',
    	"info":     false,
    	"searching": true,
    	stateSave: true,
    	"lengthMenu": [ 10, 25, 50, 75, 100, 200, 500 ],
    	"pageLength": 50,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/"+locale+".json"
        },
		"columns": [
		    { "orderable": true }, { "orderable": true },
		    { "orderable": true }, { "orderable": false }
		]
    } );

   	//Role Table
    $('#role-table').DataTable( {
    	"dom": 'l<"inner_search"f>pt',
    	"info":     false,
    	"searching": true,
    	stateSave: true,
    	"lengthMenu": [ 10, 25, 50, 75, 100, 200, 500 ],
    	"pageLength": 50,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/"+locale+".json"
        },
		  "columns": [
		    { "orderable": true }, { "orderable": true },
		    { "orderable": false }
		  ]
    } );

    //Rights Table
    $('#rights-table').DataTable( {
    	"dom": 'l<"inner_search"f>pt',
    	"info":     false,
    	"searching": true,
    	stateSave: true,
    	"lengthMenu": [ 10, 25, 50, 75, 100, 200, 500 ],
    	"pageLength": 50,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/"+locale+".json"
        },
		"columns": [
		    { "orderable": true }, { "orderable": false }, 
		    { "orderable": false },{ "orderable": false }, 
		    { "orderable": false },{ "orderable": false },
		]
    } );

    //user Table
    $('#admin-users-table').DataTable( {
    	"dom": 'l<"inner_search"f>pt',
    	"info":     false,
    	"searching": true,
    	stateSave: true,
    	"lengthMenu": [ 10, 25, 50, 75, 100, 200, 500 ],
    	"pageLength": 50,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/"+locale+".json"
        }
    } );



    //Role Rights Modal. Give values to inputs (modal.php).
	$(document).on("click", ".edit-modal-role-right", function () {
	     var input = $(this).data('id');
	     var array = input.split(" ");
	     $("#edit-modal-role-right-code").val(array[0]);
	     $("#edit-modal-role-right-name").val(array[1]);
	     $("#edit-modal-role-right-page").val(array[2]);
	     //Hidden
	     $("#edit-modal-hidden-module-code").val(array[0]);
	});

	//Role Rights Modal. Choose the right Role_Code for delete (modal.php).
	$(document).on("click", ".delete-modal-role-right", function () {
	     var input = $(this).data('id');
	     //Hidden
	     $("#delete-modal-hidden-role-right-code").val(input);
	});
	// //Module Table
	// $(document).on("click", "#add-module", function () {
	// 	   var html = '<tr>';
	// 	   html += '<td contenteditable id="data1"></td>';
	// 	   html += '<td contenteditable id="data2"></td>';
	// 	   html += '<td contenteditable id="data3"></td>';
	// 	   html += '<td style="padding: 0px;"><button type="button" name="module-insert" id="module-insert" class="btn btn-success">Προσθήκη</button></td>';
	// 	   html += '</tr>';
	// 	   $('#moduletable tbody').prepend(html);
	// });

	// //Module Table
	// $(document).on("click", "#module-insert", function () {

	// });


	function get_current_locale() {
		var locale = document.getElementById('current_locale').value;
		locale = (locale === 'en') ? 'English' : 'Greek';
		return locale;
	}
});
// //////////////////////////////////////////////////////////////////////////////////

// $(document).on("click", "#delete_module", function () {

//      var att = $(this).attr("rel");
//      // $(".modal-body #bookId").val( myBookId );
//      // As pointed out in comments, 
//      // it is superfluous to have to manually call the modal.
//      $('.modal-body #delete-input-modal').val(att);
//      $('.modal-body #delete-modal-value').val(att);
//      $('.modal-body #delete-modal-value').text(att);
// });

	// $(document).ready(function(){
	//     var locale = document.getElementById('current_locale').value;
	// 	locale = (locale === 'el') ? 'Greek': 'English';

	//   	fetch_data();

	//  	function fetch_data() {	
	// 		var dataTable = $('#moduletable').DataTable({
	// 		    "processing" : true,
	// 		    "serverSide" : true,
	// 		    "order" : [],
	// 		    "ajax" : {
	// 		     url:"fetch.php",
	// 		     data: {function2call: 'test'},
	// 		     type:"POST"
	// 	    	},
	// 	        "dom": 'l<"inner_search"f>t<"pagination"p>',
	// 	    	"info":     false,
	// 	    	"searching": true,
	// 	    	stateSave: true,
	// 	    	"lengthMenu": [ 10, 25, 50, 75, 100, 200, 500 ],
	// 	    	"pageLength": 50,
	// 	        "language": {
	// 	            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/"+locale+".json"
	//         	}
	//    		});
	//   	}

	//     function update_data(id, column_name, value) {
	// 	   $.ajax({
	// 	    url:"update.php",
	// 	    method:"POST",
	// 	    data:{id:id, column_name:column_name, value:value},
	// 	    success:function(data)
	// 	    {
	// 	     $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
	// 	     $('#moduletable').DataTable().destroy();
	// 	     fetch_data();
	// 	    }
	// 	   });
	// 	   setInterval(function(){
	// 	    $('#alert_message').html('');
	// 	   }, 5000);
	// 	}
	  
	// 	$(document).on('blur', '.update', function(){
	// 		var id = $(this).data("id");
	// 		var column_name = $(this).data("column");
	// 		var value = $(this).text();
	// 		update_data(id, column_name, value);
	// 	});
	  
	//     $('#add').click(function(){
	// 	   var html = '<tr>';
	// 	   html += '<td contenteditable id="data1"></td>';
	// 	   html += '<td contenteditable id="data2"></td>';
	// 	   html += '<td contenteditable id="data3"></td>';
	// 	   html += '<td><button type="button" name="insert" id="insert" class="btn btn-success btn-xs">Insert</button></td>';
	// 	   html += '</tr>';
	// 	   $('#moduletable tbody').prepend(html);
	//     });

	// 	$(document).on('click', '#insert', function(){
	// 		var first_name = $('#data1').text();
	// 		var last_name = $('#data2').text();
	// 		var c = $('#data3').text();

	// 		if(first_name != '' && last_name != '')	{
	// 			$.ajax({
	// 				url:"insert.php",
	// 				method:"POST",
	// 				data:{first_name:first_name, last_name:last_name, c:c},
	// 				success:function(data)
	// 				{
	// 			 	$('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
	// 			 	$('#moduletable').DataTable().destroy();
	// 			 	fetch_data();
	// 				}
	// 			});
	// 			setInterval(function(){
	// 			 	$('#alert_message').html('');
	// 			}, 5000);
	// 		}
	// 		else {
	// 			alert("Both Fields is required");
	// 		}
	//   	});

	// 	$(document).on('click', '.delete', function(){
	// 	   var id = $(this).attr("id");
	// 	   if(confirm("Are you sure you want to remove this?"))
	// 	   {
	// 	    $.ajax({
	// 	     url:"delete.php",
	// 	     method:"POST",
	// 	     data:{id:id},
	// 	     success:function(data){
	// 	      $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
	// 	      $('#moduletable').DataTable().destroy();
	// 	      fetch_data();
	// 	     }
	// 	    });
	// 	    setInterval(function(){
	// 	     $('#alert_message').html('');
	// 	    }, 5000);
	// 	   }
	//   	});

	// });

