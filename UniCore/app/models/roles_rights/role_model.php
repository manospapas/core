<?php
	
	class RoleModel extends RoleAccessModel {
	
		//We do not want GOD be able to modified by anyone!
		//TODO add a NOT after 'role_code IN'
		protected const SELECT_ROLES = "SELECT role_code, role_name FROM roles ";#WHERE role_code IN ('GOD',?)";	 
		protected const INSERT_ROLE = 'INSERT INTO roles VALUES (?, ?)';
		protected const UPDATE_ROLE = 'UPDATE roles SET role_code = ?, role_name = ? WHERE role_code = ?';
		protected const DELETE_ROLE = 'DELETE FROM roles WHERE role_code = ?';
		protected const SELECT_ALL_ROLES = "SELECT * FROM roles WHERE role_code != 'GOD' ORDER BY role_name";

		public function __construct() {
			parent::__construct();
		}

		public function show_roles($except_current_role) {
			return $this->execute_query_and_fetch_result(self::SELECT_ROLES, [$except_current_role]);
		}

		public function insert_role($role_code, $role_name) {
			$this->execute_query(self::INSERT_ROLE, [$role_code, $role_name]);
		}

		public function update_role($role_code, $role_name, $current_module_code) {
			$this->execute_query(self::UPDATE_ROLE, [$role_code, $role_name, $current_module_code]);
		}

		public function delete_role($role_code) {
			$this->execute_query(self::DELETE_ROLE, [$role_code]);
		}

		public function get_all_roles() {
			return $this->execute_query_and_fetch_result(self::SELECT_ALL_ROLES);
		}

	}
?>