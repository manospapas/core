<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/app/models/database.php';
	
	class RoleAccessModel extends Database {

		protected const READ_RIGHTS        = 'SELECT * FROM role_rights where role_right_code = ? order by role_right_module_code'; 
		protected const SELECT_MODULE_CODE = 'SELECT module_code FROM modules WHERE module_page = ?';
		protected const HAS_ACCESS         = 'SELECT * from role_rights where role_right_role_code = ? and role_right_module_code = ?';
		protected const MENU_ITEMS         = 'SELECT * FROM menu_items where menu_item_visibility like ? order by  menu_item_parent, menu_item_position';
		protected const GET_ALL_ROLES      = 'SELECT role_code FROM roles';


		public function __construct() {
			parent::__construct();
		}

		public function get_page_module_code($file) {
			$result = $this->execute_query_and_fetch_result(self::SELECT_MODULE_CODE, [$file]);
			$module_code = $this->fetch_result($result);
			return $module_code['module_code'];
		}

		public function allows_access($role, $file) {
			$module = $this->get_page_module_code($file);
			$result = $this->execute_query_and_fetch_result(self::HAS_ACCESS, [$role, $module]);
			return $this->fetch_result($result);
		}

		public function get_menu_items($role) {
			return $this->execute_query_and_fetch_result(self::MENU_ITEMS, ["%$role%"]);
		}	
		protected function get_all_roles() {
			return $this->execute_query_and_fetch_result(self::GET_ALL_ROLES);
		}	

	}

?>