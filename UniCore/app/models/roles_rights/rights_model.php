<?php

	//require ($_SERVER['DOCUMENT_ROOT'] ."/app/models/roles_rights/module_model.php");

	class RightsModel extends RoleAccessModel {

		protected const READ_RIGHTS                     = 'SELECT * FROM role_rights where role_right_role_code = ? order by role_right_module_code';
		protected const INSERT_RIGHTS                   = 'INSERT INTO role_rights (role_right_role_code, role_right_module_code, role_right_add, role_right_edit, role_right_delete, role_right_view, role_right_row_access) VALUES (?, ?, 0, 0, 0, 0, 0)';
		protected const DELETE_RIGHTS                   = 'DELETE FROM role_rights WHERE role_right_role_code = ?';
		protected const UPDATE_ROLE_CODE_IN_ROLE_RIGHTS = 'UPDATE role_rights SET role_right_role_code = ? WHERE role_right_role_code = ?';

		public function __construct() {
			parent::__construct();
		}
		
		public function read_rights($role){
			return $this->execute_query_and_fetch_result(self::READ_RIGHTS, [$role]);
		}

		public function insert_rights($role_code) {
			$modules = (new ModuleModel())->get_all_modules();

			foreach($modules as $module) {
				$this->execute_query(self::INSERT_RIGHTS, [$role_code, $module['module_code']]);
			}
		}
		
		public function update_rights($set, $id) {
			$sql = "UPDATE role_rights SET $set WHERE role_right_id = ?";
			$this->execute_query($sql, [$id]);
		}

		public function update_role_in_role_rights($new_role_code, $current_role_code) {
			$this->execute_query(self::UPDATE_ROLE_CODE_IN_ROLE_RIGHTS, [$new_role_code, $current_role_code]);
		}

		public function delete_rights($role_code) {
			$this->execute_query(self::DELETE_RIGHTS, [$role_code]);
		}

	}

?>