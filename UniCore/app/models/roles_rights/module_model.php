<?php 
	
	class ModuleModel extends RoleAccessModel {

		protected const SELECT_MODULES                = 'SELECT * FROM modules';
		protected const UPDATE_MODULE                 = 'UPDATE modules SET module_code = ?, module_name = ?, module_page = ? WHERE module_code = ?';
		protected const INSERT_MODULE_IN_ROLE_RIGHTS  = 'INSERT INTO role_rights (role_right_role_code, role_right_module_code, role_right_add, role_right_edit, role_right_delete, role_right_view, role_right_row_access) VALUES (?, ?, ?, ?, ?, ?, ?)';
		protected const DELETE_MODULE                 = 'DELETE FROM modules WHERE module_code = ?';
		protected const DELETE_MODULES_FROM_RIGHTS    = 'DELETE FROM role_rights WHERE role_right_module_code = ?';
		protected const UPDATE_ROLE_RIGHT_MODULE_CODE = 'UPDATE role_rights SET role_right_module_code = ? WHERE role_right_module_code = ?';
		protected const INSERT_MODULE                 = 'INSERT INTO modules VALUES (?, ?, ?)';

		public function __construct() {
			parent::__construct();
		}

		public function get_all_modules() {
			return $this->execute_query_and_fetch_result(self::SELECT_MODULES);
		}

		public function insert_module($module_code, $module_name, $module_page) { 
			return $this->execute_query(self::INSERT_MODULE, [$module_code, $module_name, $module_page]);
		}

		public function insert_module_in_role_rights($module_code) {
			$roles = $this->get_all_roles();
			foreach ($roles as $role) {
				$access = ($role['role_code'] === 'GOD') ? 1 : 0;
				$successful_action = $this->execute_query(self::INSERT_MODULE_IN_ROLE_RIGHTS, [$role['role_code'], $module_code, $access, $access, $access, $access, $access]);
				
				// if(!($successful_action instanceof PDOStatement))
				// 	return false;
			}
		}
		
		public function update_module($module_code, $module_name, $module_page, $current_module_code) {
			return $this->execute_query(self::UPDATE_MODULE, [$module_code, $module_name, $module_page, $current_module_code]);
		}

		public function update_role_right_module_code($module_code, $current_module_code) {
			return $this->execute_query(self::UPDATE_ROLE_RIGHT_MODULE_CODE, [$module_code, $current_module_code]);
		}


		public function delete_module($module_code) {
			return $this->execute_query(self::DELETE_MODULE, [$module_code]);
		}

		public function delete_modules_from_rights($module_code) {
			return $this->execute_query(self::DELETE_MODULES_FROM_RIGHTS, [$module_code]);
		}


	}

?>