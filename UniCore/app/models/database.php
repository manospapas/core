<?php


	// Include files.
	require_once($_SERVER['DOCUMENT_ROOT'] .'/config/db_configuration.php');
	require_once($_SERVER['DOCUMENT_ROOT'] .'/public/core.php');

	/*
		Creates an object of Database, which allows the system to connect to the database and make sql executions.
	*/

	class Database extends Core{

		// Attributes  /////////////////////////////////////////////////////////////////////////////////
			/*
				(PDO) $handler: this attribute is the connection to the database itself. It 'handles' the connection with the database.
			*/
			protected $handler = null;
			protected $host, $username, $db_password, $db_name, $driver, $charset;

		// CONSTRUCTOR AND DESTRUCTOR  /////////////////////////////////////////////////////////////////////////////////
			public function __construct($host = '', $username = '', $db_password = '', $db_name = '', $driver = '', $charset = '') {
	        	$this->host        = empty($host)        ? $GLOBALS['host']        : $host;
	        	$this->username    = empty($username)    ? $GLOBALS['username']    : $username;
	        	$this->db_name     = empty($db_name)     ? $GLOBALS['db_name']     : $db_name;
	        	$this->driver      = empty($driver)      ? $GLOBALS['driver']      : $driver;
	        	$this->db_password = empty($db_password) ? $GLOBALS['db_password'] : $db_password;
	        	$this->charset     = empty($charset)     ? $GLOBALS['charset']     : $charset;
	        	$this->database_connection();
			}

			public function __destruct() { unset($this->handler); }

		// GETERS AND SETTERS  ///////////////////////////////////////////////////////////////////////////////////////// 
		    protected function get_driver() { return $this->driver; }

		    protected function get_host() { return $this->host; }

		    protected function get_db_name() { return $this->db_name; }

		    protected function get_username() { return $this->username; }

		    protected function get_db_password() { return $this->db_password; }

		    protected function get_charset() { return $this->charset; }

		    protected function get_handler() { return $this->handler; }

		// CORE FUNCTIONS  /////////////////////////////////////////////////////////////////////////////////////////////
			/*
				Connects to the database.
				Returns:
					(PDO) A PDO object that will handle the database interactions.
			*/
			protected function database_connection() {
				try {
					$handler = new PDO(
						"$this->driver:host=$this->host;dbname=$this->db_name; charset=$this->charset",
						$this->username, $this->db_password,
						array(
	                        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
	                        \PDO::ATTR_PERSISTENT => false
	                    ));
				} 
				catch(PDOException $e) {
					echo 'ERROR: ' . $e->getMessage();
					die();
				}

				return $this->handler = $handler;
			}

			// CRUD /////////////////////////////////////////////////////////////////////////////////////////////
			    /*
			    	Inserts values to database.
			    	Params:
			    		(STRING) $table: the name of the table that we want to insert.
			    		(ARRAY) $keys_values: an array with key and value pairs. For example: array("key1" => "value1", "key2" => "value2", "key3" => "value3").
			    	Returns:
			    		(PDO) A PDO object.
			    	Notice: You do NOT have to give a key value for all columns of the table.
			    	Hint: 
			    		Assuming that you have something like $result = $db->database_select("..."); $result->rowCount();, then $result will return 1 for success, 0 for failure and -1 if something went completely wrong. Therefore, you can use to check what happend without making a query to the database.
			    	WARNING!!!
			    		This method can be used only if the table has the first column as ID.
			    		Moreover, you can not add NULL values if an value is required.
			    		There are no checks if your insertion is correct. If it is not, the executation will fail.
			    */
			    public function database_insert($table, array $keys_values) {

			    	if($this->table_check($table) !== -1) {   	
						$this->insert_column_names[] = $this->get_column_names($table);
						//Remove ID, ID is always the first element! ID is autocreased in database, therefore we must not do any manipulation.
				        unset($this->insert_column_names[0]);

						$set_columns = '';
						$set_values  = '';
						$bind_values = array();

						foreach($keys_values as $column_name => $value) {
							$set_columns   = "$column_name, $set_columns";
							$set_values    = "?, $set_values";
							$bind_values[] = $value;;
						}

						$set_columns = $this->remove_last_comma($set_columns);
						$set_values  = $this->remove_last_comma($set_values);

						$sql         = "INSERT INTO $table ($set_columns) VALUES ($set_values);";
						// Reverse array $bind_values, since the elements are in reverse order.
						$bind_values = array_reverse($bind_values);	
						return       $this->execute_query($sql, $bind_values);
					}
					else return -1;				
				}

				/* 
					Deletes a row from database.
					Params:
						(STRING) $table: the name of the table that we want to insert.
						(ARRAY) $id: the ID of the rows.
					Returns:
						(PDO) A PDO object.
			    	Hint: 
			    		Assuming that you have something like $result = $db->database_delete("..."); $result->rowCount();, then $result will return 1 for success, 0 for failure and -1 if something went completely wrong. Therefore, you can use to check what happend without making a query to the database.
					WARNING!!!
			    		This method can be used only if the table has the first column as ID.
				*/
				public function database_delete($table, array $ids) {
					if($this->table_check($table)){
						$this->column_names[] = $this->get_column_names($table);
						$column_id            = $this->column_names[0][0];
						$set_bind             = '';

						foreach($ids as $id) {
							$set_bind .= '?,';
						}

						$set_bind    = $this->remove_last_comma($set_bind);
						$sql         = "DELETE FROM $table WHERE $column_id IN (" . $set_bind . ");";
						$bind_values = $ids;
						return       $this->execute_query($sql, $bind_values);				
					}
					else return -1;
				}

				/*
					Updates a row from database.
					Params:
						(STRING) $table: the name of the table that we want to insert.
						(ARRAY) $id: the ID of the row.
						(ARRAY) $keys_values: an array with key and value pairs. For example: array("key1" => "value1", "key2" => "value2", "key3" => "value3")
					Returns:
						(PDO) A PDO object.
			    	Hint: 
			    		Assuming that you have something like $result = $db->database_update("..."); $result->rowCount();, then $result will return 1 for success, 0 for failure and -1 if something went completely wrong. Therefore, you can use to check what happend without making a query to the database.
					WARNING!!!
			    		This method can be used only if the table has the first column as ID.
				*/
				public function database_update($table, array $ids, array $keys_values) {
					$this->update_column_names[] = $this->get_column_names($table);
					$column_id                   = $this->update_column_names[0][0];
					$set_values           	     = '';
					$bind_values                 = array();
					$bind_ids					 = '';

					foreach($keys_values as $key => $value) {
						$set_values    .= "$key = ?,";
						$bind_values[] .= $value;
					}
					foreach ($ids as $id) {
						$bind_ids .= '?,';
					}

					$bind_ids    = $this->remove_last_comma($bind_ids);
					$bind_values = $this->flatten_array(array_merge($bind_values, $ids)); //Add to bind_values the ID.
					$set_values  = $this->remove_last_comma($set_values);
					$sql         = "UPDATE $table SET $set_values WHERE $column_id IN ($bind_ids);";

					return $this->execute_query($sql, $bind_values);
				}

				/*
					Selects rows from the database.
					Params: 
						(STRING) $table: a mysql table.
						(STRING) $columns: the columns that will be selected.
						(ARRAY) $fields: an array with key values of the fields tha would be compared. For example "ID = 56" where 'ID' is the key and '56' is the value.
						(ARRAY) $operators: a non key value array with the operators '> = IN <'. The operators should be provided as strings.
						(ARRAY) $order_by: the columns that we want to order by.
						(INT) $limit: the limit of the results.
					Returns:
						(PDO) PDO objects.
					Notice:
						!!! The $operators must be double the sized of the $fields, since we want an operator for every field.
						!!! The last operator must be ' '.
			    	Hint: 
			    		Assuming that you have something like $result = $db->database_select("..."); $result->rowCount();, then $result will return 1 for success, 0 for failure and -1 if something went completely wrong. Therefore, you can use to check what happend without making a query to the database.

			    		Use Example: database_select('users', array('*'), array('first_name'=>'Panos', 'last_name'=>'Dimirakopoulos'), 
						array('=','or', '=',' '), ('last_name', DESC), 100); 
						Equal SQL statement:
							'SELECT * FROM users WHERE first_name='Panos' OR last_name='Dimirakopoulos' ORDER BY last_name DESC limit 100;
					WARNING:
						You can NOT make all kind of select.
				*/
				public function database_select($table, $columns, array $fields = [], array $operators = [], array $order_by = [], $limit = 1000000000) {
					if(sizeof($fields) == sizeof($operators)/2) {
						$columns = implode(",", $columns);
						$bind_values = array();
						$sql = "SELECT $columns FROM $table ";

						if(!empty($fields) && !empty($operators)){
							$sql .= ' WHERE ';

							for ($x = 0; $x < sizeof($fields); $x++){
								$sql .= key($fields). ' ' . ($operators[$x*2]) . ' ? ' . ($operators[$x*2+1]). ' ';
								$bind_values[] = current($fields); 
								next($fields);
							}
						}

						if(!empty($order_by)) {
							$order = '';
							$numItems = count($order_by);
							$i = 0;

							foreach ($order_by as $key => $value) {
								if($value === 'DESC' || $value === 'ASC') {
									$order = $this->remove_last_comma($order);
									$order .= ' '.$value;
								}
								else {
									$order .= (++$i === $numItems) ? $value : $value . ','; 
								}
							}

							$sql .= " ORDER BY $order ";
						}

						$sql .= "LIMIT $limit;";
						$result = $this->execute_query($sql, $bind_values);
						return $result;
					}
				}

			// Execution
				/*
					This method has the ability to execute any sql querry that is given.
					Params:
						(STRING) $sql: a string that contains the sql statement. For instance "UPDATE users SET role = 'some_new_role' WHERE id = 39". 
						(ARRAY) $bind_values: the values that you want to bind. Binding the values provides security for SQL injection.
					Returns:
						(PDO) the PDO object OR -1 if something went completely wrong.
			    	Hint: 
			    		Assuming that you have something like $result = $db->execute_query("..."); $result->rowCount();, then $result will return 1 for success, 0 for failure and -1 if something went completely wrong. Therefore, you can use to check what happend without making a query to the database.
						
				*/
				public function execute_query_and_fetch_result($sql, array $bind_values = [] ) {
					try {
						$query = $this->get_handler()->prepare($sql);
						//Bind the values for security reasons.
						$query->execute($bind_values);
					}catch(PDOException $e) { 
						echo '<br>'.'<b>ERROR:</b> ' . $e->getMessage().'<br>';
						echo 'SQL statement: '.$sql; 
					}
					finally { 
						return  isset($query) ? $query : -1; 
					}
				}

				/*
					Same as execute_query_and_fetch_result, but returns true or false.
				*/
				public function execute_query($sql, array $bind_values = [] ) {
					try {
						$query = $this->get_handler()->prepare($sql);
						//Bind the values for security reasons.
						$success = $query->execute($bind_values);
					}catch(PDOException $e) { 
						echo '<br>'.'<b>ERROR:</b> ' . $e->getMessage().'<br>'; 
						echo 'SQL statement: '.$sql;
					}
					finally { 
						return  $success;
					}
				}

		// HELPING METHODS  /////////////////////////////////////////////////////////////////////////////////////////
			/*
				Iterates through results.
				Params: 
					(STRING) $table: a mysql table.
				Returns:
					(ARRAY) An array with all results from the query.
			*/
			// This method can co-operate with function iterate_through_results($table).
			public function fetch_all_results($table) {
				$query = $this->get_handler()->query("SELECT * FROM $table");
				$results = $query->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}

			/*
				Fetches the result from an sql statement or a PDO object.
				Params:
					(STRING) $sql: a sql statement.
					(PDO) $pdo_object: a PDO object. 
				Returns: 
					(ARRAY) An array with all results.
				Notice:
					If you give both arguments the $sql will prevail.
					Notice if you want to use the $pdo_object as parameter give the $sql as empty, for instance fetch_result('', $pdo)'.
					Use $pdo_object if you have a PDO object already, which means less querries in the database.
			*/
			public function fetch_result($pdo_object = '', $sql = '') {
				if(empty($sql) && empty($pdo_object)) return [];
				$runner = (empty($sql)) ? $pdo_object : $this->get_handler()->query($sql);
				$results = $runner->fetch(PDO::FETCH_ASSOC);
				return $results;
			}

			/*
				Iterates through the results.
				Params:
					(STRING) $table: a mysql table.
					(BOOLEAN) $ids: true if you want to include ids in the results, otherwise false.
					(BOOLEAN) $keys  : the column names
					(BOOLEAN) $values: the values from the rows.
				Returns:
					(ARRAY) An array with all results.
				Notice:
					There is no much point of returning the $keys, but we allow it.
			*/
			//  Co-operates with the function fetch_all_results($table).
			public function iterate_through_results($table, $ids = false, $keys = false, $values = true) {
				$results = [];
				$row = 0;

				foreach ($this->fetch_all_results("$table") as $result => $array) {
					$temp_array = [];
					$i = 0;
		    		$first = -1;
		    		foreach ($array as $key => $value) {
		    			$first++;
		    			if(!$ids && $first == 0)  continue;

		    			if($values && !$keys)     $temp_array[$i++] = $value;
		    			elseif(!$values && $keys) $temp_array[$i++] = $key;
		    			elseif($values && $keys)  $temp_array[$i++] = [$key => $value];
		    			else                      return [];
		    		}
		    		$results[$row++] = $temp_array;
				}

				return $results;
			}

			/*
				Echoes the results from a given table.
				Params:
					(STRING) $table: a mysql table.
					(BOOLEAN) $ids: true if you want to include ids in the results, otherwise false.
					(BOOLEAN) $keys  : the column names
					(BOOLEAN) $values: the values from the rows.
				Notice:
					There is no point of returning the $keys, but it is allowed.
			*/
			// This method prints out the whole table. It uses the show_fetch_results method to retrieve the results.
			public function show_fetch_results( $table, $ids = false, $keys = false, $values = true) {

				$results = $this->iterate_through_results($table, $ids, $keys, $values);	

				if($keys xor $values){
				    foreach ($results as $result) {
				    	$i = -1;
				    	foreach ($result as $key => $value) {
				    		$i++;
				    		if(!$ids && $i == 0) continue;
				    		echo '<br>' . $value;
				      	}
				      	echo '<br>';
				      	if(!$values) return;     
				    }
				}
				elseif($keys && $values) {
					foreach ($results as $array) {
						$i = -1;
	    				foreach ($array as $result) { 					
	      					foreach ($result as $key => $value) {
	      						$i++;
	      						if(!$ids && $i == 0) continue;
	        					echo $key. ':'.$value.'<br>';
	      					}
						}
	 				}
				}
			}

			/*
				Fetches the results as objects.
				Params:
					(STRING) $sql: a sql statement.
				Returns:
					(ARRAY) An array of PDO objects.

			*/
			public function fetch_object_results($sql) {
				$query   = $this->execute_query($sql);
				$results = [];
				$i       = 0;

				while($row = $query->fetch(PDO::FETCH_OBJ)) { 
					$results[++$i] = $row; 
				}

				return $results;
			}

			/*
				Returns the last inserted id.
				Returns:
					(INTEGER) The last inserted id.
				WARNING:
					No other sql action must take place!!! If another sql execution happens, then 0 will be returned.
			*/
			//  Returns the last inserted id. Useful if you need to know the last insert item.
			//WARNING: No other sql action must take place!!! If another sql execution happens, then 0 will be returned.		
			public function last_inserted_id() { return $this->get_handler()->lastInsertId(); }

			/*
				Gets the column names of a given mysql table.
				Params:
					(STRING) $table: a mysql table.
				Returns:
					(ARRAY) An array with the column names. 
			*/
			public function get_column_names($table) {
		        $sql = 'SHOW COLUMNS FROM ' . $table;	        
		        $this->statement = $this->get_handler()->prepare($sql);
		            
		        try {    
		            if($this->statement->execute()) {
		                $raw_column_data = $this->statement->fetchAll();
		                
		                foreach($raw_column_data as $outer_key => $array) {
		                    foreach($array as $inner_key => $value ){	                           
		                        if ($inner_key === 'Field') {
		                            if (!(int)$inner_key){
		                            	// Store all column names in an array.
		                                $this->get_column_names[] = $value;
		                            }
		                        }
		                    }
		                }        
		            }
		            return $this->get_column_names;
		        } 
		        catch (Exception $e) {
		            return $e->getMessage();
		        }        
		    }

			/*
				Encrypts a given password.
				Params:
					(STRING) $password: a given password.
				Returns:
					(STRING) A password with hash and salt. 
				Notice:	
					Hash and salt your password.
			*/
			public function encrypt_password($password) {			
				return password_hash($password, PASSWORD_DEFAULT, array('cost' => 12));
			}

			/*
				Generates a random hash.
				Returns:
					(STRING) A random hash.
			*/
			public function generate_hash() {
				return password_hash(rand(rand(0,100000),rand(0,100000)), PASSWORD_DEFAULT, ['cost' => 12]);
			}

			/*
				Checks if the given table is one word only.
				Params: 
					(STRING) $table: a mysql table.
				Returns:
					(STRING) A table, or -1 if the $table size is bigger than 1.
			*/
			protected function table_check($table) {
				//Security reasons, making sure that $table is one word only.
		    	$table = explode(' ',trim($table));
		    	if(sizeof($table) == 1) {
		    		return $table;
		    	}
		    	else return -1; // Probably someone is trying to fidn a hole in the system.

			} 



	}
	

?>