<?php

	require_once ($_SERVER['DOCUMENT_ROOT'] ."/app/models/database.php");

	class UserModel extends Database{
		protected $name, $email, $role, $hash_code, $password;
		private const SELECT_STATEMENT = 'SELECT * FROM users WHERE user_email=?';
		private const RESET_STATEMENT = 'SELECT * FROM users WHERE user_email=? AND user_hash_code=?';
		private const UPDATE_STATEMENT = 'UPDATE users SET user_password=?, user_hash_code=? WHERE user_email=?';
		private const SELECT_ALL_USERS = 'SELECT * FROM users';
		private const INSERT_NEW_USER = 'INSERT INTO users (user_name, user_first_name, user_last_name, user_email, user_password, user_role, user_hash_code) VALUES (?, ?, ?, ?, ?, ?, ?)';
		private const DELETE_USER = 'DELETE FROM users WHERE id=?';

		public function __construct($username = '', $email = '', $role = '', $password = '') {
			$this->username = $username;
			$this->email = $email;
			$this->role = $role;
			$this->password = $password;
			parent::__construct();
		}

		protected function get_username() {
			return $this->username;
		}

		protected function set_username($username) {
			$this->username = $username;
		}

		protected function get_email() {
			return $this->email;
		}

		protected function set_email($email) {
			$this->email = $email;
		}

		protected function get_role() {
			return $this->role;
		}

		protected function set_role($role) {
			$this->role = $role;
		}

		protected function get_password() {
			return $this->password;
		}

		protected function set_password($password) {
			$this->password = $password;
		}

		public function email_exist($email) {
			return $this->execute_query_and_fetch_result(self::SELECT_STATEMENT,[$email]);
		}

		public function reset_password($new_password, $hash_code, $email) {
			return $this->execute_query(self::UPDATE_STATEMENT, [$new_password, $hash_code, $email]);
		}

		public function valid_arguments($email, $hash) {
			return $this->execute_query(self::RESET_STATEMENT,[$email,$hash]);
		}

		public function get_all_users() {
			return $this->execute_query_and_fetch_result(self::SELECT_ALL_USERS);
		}

		public function insert_new_user($username, $firstname, $lastname, $email, $password, $role) {
			$password = $this->encrypt_password($password);
			$hash_code = $this->generate_hash();
			$this->execute_query(self::INSERT_NEW_USER, [$username, $firstname, $lastname, $email, $password, $role, $hash_code]);
		}

		public function delete_user($id) {
			return $this->execute_query_and_fetch_result(self::DELETE_USER, [$id]);
		}

	}


?>