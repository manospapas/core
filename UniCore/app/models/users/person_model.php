<?php

	class PersonModel extends UserModel{
		private $first_name, $last_name
		private const SELECT_STATEMENT = 'SELECT * FROM users WHERE user_email=?';
		private const RESET_STATEMENT = 'SELECT * FROM users WHERE user_email=? AND user_hash_code=?';
		private const UPDATE_STATEMENT = "UPDATE users SET user_password=?, user_hash_code=? WHERE user_email=?";

		public function __construct($first_name = '', $last_name = '') {
			parent::__construct();
			$this->first_name = $first_name;
			$this->last_name = $last_name;
		}

		private function get_first_name() {
			return $this->first_name;
		}

		private function set_first_name($first_name) {
			$this->first_name = $first_name;
		}

		private function get_last_name() {
			return $this->last_name;
		}

		private function set_last_name($last_name) {
			$this->last_name = $last_name;
		}

	}


?>