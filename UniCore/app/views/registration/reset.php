<?php


  require_once dirname(__DIR__).'/front/layout.php';

  $texts = array(
    'url_params' => $core->url_params(),
    'locale' => $core->current_locale(),
    'header' => $text['reset']['header'],
    'language' => $text['common']['language'],
    'greek' => $text['common']['greek'],
    'english' => $text['common']['english'],
    'password' => $text['reset']['password'],
    'repeat_password' => $text['reset']['repeat_password'],
    'submit' => $text['reset']['submit'],
    'login' => $text['reset']['login']

  );
 
  $email = isset($_GET['email']) ? $_GET['email'] : "";
  $hash  = isset($_GET['hash']) ? $_GET['hash'] : "";

  if(!empty($_POST['new-password']) && !empty($_POST['new-password-repeat'])) {

    if(!empty($_GET['email']) && !empty($_GET['hash'])) {
      $current_user->reset_password($_GET['email'], $_GET['hash'], $_POST['new-password'], $_POST['new-password-repeat']);
    }
    $texts['message'] = $current_user->get_message();
  }
  else {
    $texts['message'] = $text['reset']['message'];
  }

  //$texts['params'] = 'reset.php?email='. $email .'&hash='. $hash;
  echo $twig->render('registration/reset.html', $texts);


?>