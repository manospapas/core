<?php 
 
	require_once dirname(__DIR__).'/front/layout.php';

	$texts = array(
		'locale' => $core->current_locale(),
		'url_params' => $core->url_params(),
	    'header' => $text['forgot']['header'],
	    'language' => $text['common']['language'],
	    'greek' => $text['common']['greek'],
	    'english' => $text['common']['english'],
	    'email' => $text['forgot']['email'],
	    'placeholder_email' => $text['forgot']['placeholder_email'],
	    'reset_password' => $text['forgot']['reset_password'],
	    'login_page' => $text['forgot']['login_page']	    
 	 );

  
	if(isset($_POST['email'])) {
		$current_user->forgot_password($_POST['email']);
		$texts['message'] = $current_user->get_message();		
	}
	else $texts['message'] = $text['forgot']['message'];

	echo $twig->render('registration/forgot.html', $texts); 

?>