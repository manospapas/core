<?php
 
	require_once dirname(__DIR__).'/front/layout.php';
	
	$current_user->logout();
	header("Location: /app/views/registration/login.php?language=". $_SESSION['language']);

?>