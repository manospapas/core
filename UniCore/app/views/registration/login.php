<?php

  require_once dirname(__DIR__).'/front/layout.php';

  $texts = array(
    'url_params' => $core->url_params(),
    'locale' => $core->current_locale(),
    'header' => $text['login']['header'],
    'language' => $text['common']['language'],
    'greek' => $text['common']['greek'],
    'english' => $text['common']['english'],
    'email' => $text['login']['email'],
    'placeholder_email' => $text['login']['placeholder_email'],
    'password' => $text['login']['password'],
    'placeholder_password' => $text['login']['placeholder_password'],
    'remember_password' => $text['login']['remember_password'],
    'forgot_password' => $text['login']['forgot_password'],
    'login' => $text['login']['login']
  );


  if(isset($_POST['email']) && isset($_POST['password'])) {
    $current_user->login($_POST['email'], $_POST['password']);
    $texts['message'] = $current_user->get_message();
    
  }
  echo $twig->render('registration/login.html',$texts);

?>