<?php

	session_start();
	ini_set('display_errors', 'On');
	define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT']); // Full Root.
	require_once ROOT_PATH.'/vendor/autoload.php';
	require_once ROOT_PATH.'/app/controllers/users/current_user_controller.php';
	require_once ROOT_PATH.'/app/controllers/roles_rights/role_access_controller.php';
	require_once ROOT_PATH.'/app/controllers/roles_rights/module_controller.php';
	require_once ROOT_PATH.'/app/controllers/roles_rights/rights_controller.php';
	require_once ROOT_PATH.'/app/controllers/roles_rights/role_controller.php';
	
	$loader = new Twig_Loader_Filesystem( ROOT_PATH.'/templates');
	$twig   = new Twig_Environment($loader);
	$twig->addExtension(new Twig_Extension_StringLoader());

	$core              = new Core();
	$current_user      = new CurrentUserController();
	$role_access       = new RoleAccessController();
	$module_controller = new ModuleController();
	$role_controller   = new RoleController();
	$rights_controller = new RightsController();
	$rights_controller = new RightsController();
	$user_controller   = new UserController();
	
	$language = (isset($_GET['language'])) ? $core->language_switcher($_GET['language']) : $core->language_switcher();
	require $_SERVER['DOCUMENT_ROOT'].'/config/locales/admin/'. $language .'/_main.xml.php';

?>