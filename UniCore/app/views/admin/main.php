<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/app/views/share/load.php';

	$file_name = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

	//TODO what is the main for each role?
	if($file_name === 'main.php' || $role_access->has_access($current_user, $file_name)) {

		$CRUD   = $role_access->get_crud_rights();
		$view   = (isset($CRUD[0])) ? $CRUD[0] : 'no-view';
		$create = (isset($CRUD[1])) ? $CRUD[1] : 'no-create';
		$edit   = (isset($CRUD[2])) ? $CRUD[2] : 'no-edit';
		$delete = (isset($CRUD[3])) ? $CRUD[3] : 'no-delete';

		$text_content = array(
			'locale' => $core->current_locale(),
			'logo' => $text['navbar']['logo'],
			'dashboard' => $text['navbar']['dashboard'],
			'exams' => $text['navbar']['exams'],
			'centers' => $text['navbar']['centers'],
			'candidate' => $text['navbar']['candidate'],
			'new' => $text['navbar']['new'],
			'old' => $text['navbar']['old'],
			'rights' => $text['footer']['rights'],
			'logout_header' => $text['topbar']['header'],
			'logout_message' => $text['topbar']['message'],
			'logout' => $text['topbar']['logout'],
			'profile' => $text['topbar']['profile'],
			'search' => $text['topbar']['search'],
			'elements' => $role_access->get_role_menu($current_user->role),
			'role' => $current_user->role, //TODO might not needed!
			'username' => $current_user->username,
			'view_class' => $view,
			'create_class' => $create,
			'edit_class' => $edit,
			'delete_class' => $delete,
			'language' => $text['common']['language'],
			'greek' => $text['common']['greek'],
			'english' => $text['common']['english'],
			'add' => $text['common']['add'],
			'cancel' => $text['common']['cancel'],
			'edit' => $text['common']['edit'],
			'delete' => $text['common']['delete'],
			'submit' => $text['common']['submit'],
			'action' => $text['common']['action']
		);
	}
	else header("Location: /app/views/admin/no_access.php");

?>