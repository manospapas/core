<?php
	
	require  $_SERVER['DOCUMENT_ROOT'].'/app/views/admin/main.php';

	if(isset($_POST['add-user-modal'])) {
		$user_controller->insert_new_user(
			$_POST['add-modal-user-sudoname'], $_POST['add-modal-user-firstname'],
			$_POST['add-modal-user-lastname'], $_POST['add-modal-user-email'],
			$_POST['add-modal-user-password'], $_POST['add-modal-user-role']
		);
	}

	// if(isset($_POST['delete-user-modal'])){
	// 	$user_controller->delete_module($_POST['delete-modal-user-id']);
	// }
 
	$text_content['all_users']   = $user_controller->get_all_users();
	$text_content['all_roles']   = $role_controller->get_all_roles();
	$text_content['id']          = $text['common']['id'];
	$text_content['sudoname']    = $text['common']['username'];
	$text_content['firstname']   = $text['common']['firstname'];
	$text_content['lastname']    = $text['common']['lastname'];
	$text_content['email']       = $text['common']['email'];
	$text_content['role']      	 = $text['common']['role'];
	$text_content['password']  	 = $text['common']['password'];
	$text_content['choose']  	 = $text['common']['choose'];
	$text_content['action_page'] = 'user.php'; //Decides the action page. It is not for view.

	$text_content['modal_message'] = $text['modal']['message'];

	echo $twig->render('admin/users/user.html', $text_content);

?>