<?php	

	require  $_SERVER['DOCUMENT_ROOT'].'/app/views/admin/main.php';

	$role = (isset($_SESSION['change_role_rights'])) ? $_SESSION['change_role_rights'] : 'Roles';
	
	if (isset($_POST['formSubmit'])) {
		$rights_controller->update_rights($role);
		header("Location: #");
	}

	if (isset($_POST['selected-role'])) {
		$role                               = $_POST['selected-role'];
		$text_content['all_modules']        = $rights_controller->show_all_role_rights($role);
		$text_content['sizeof_all_modules'] = $text_content['all_modules']->rowCount();
		$_SESSION['change_role_rights']     = $role;
	}
	else {
		$text_content['sizeof_all_modules'] = (isset($_SESSION['sizeof_all_modules'])) ? $_SESSION['sizeof_all_modules'] : 0;
	}

	$text_content['all_modules']  = $rights_controller->show_all_role_rights($role);
	$text_content['default_role'] = $role;
	$text_content['roles']        = $role_controller->show_roles($current_user->role);
	$text_content['access']       = $text['rights']['access'];
	$text_content['view']         = $text['rights']['view'];
	$text_content['row_access']   = $text['rights']['row_access'];	
	$text_content['full_access']  = $text['rights']['full_access'];	
	$text_content['no_access']    = $text['rights']['no_access'];
	$text_content['code']         = $text['module']['code'];
	$text_content['submit']       = $text['common']['submit'];
	$text_content['reset']        = $text['common']['reset'];
	$text_content['save']         = $text['common']['save'];
	$text_content['action_page']  = 'role_rights.php'; //Decides the action page. It is not for view.	

	echo $twig->render('admin/role_rights/rights.html', $text_content);

?>