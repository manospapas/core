<?php	

	require  $_SERVER['DOCUMENT_ROOT'].'/app/views/admin/main.php';

	if(isset($_POST['add-role-right-modal'])){
		$role_controller->insert_role(
			$_POST['add-modal-role-right-code'], $_POST['add-modal-role-right-name']
		); 
	}

	if(isset($_POST['edit-role-right-modal'])){
		$role_controller->update_role(
			$_POST['edit-modal-role-right-code'], $_POST['edit-modal-role-right-name'], 
			$_POST['edit-modal-hidden-module-code']
		);
	}

	if(isset($_POST['delete-module'])){
		$role_controller->delete_role($_POST['delete-modal-hidden-module-code']);
	}

	$text_content['roles']         = $role_controller->show_roles($current_user->role);
	$text_content['code']          = $text['role']['name'];
	$text_content['name']          = $text['role']['code'];
	$text_content['modal_message'] = $text['modal']['message'];
	$text_content['save']          = $text['common']['save'];
	$text_content['action_page']   = 'role.php'; //Decides the action page. It is not for view.

	echo $twig->render('admin/role_rights/role.html', $text_content);

?>