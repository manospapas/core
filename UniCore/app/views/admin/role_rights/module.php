<?php

	require $_SERVER['DOCUMENT_ROOT'].'/app/views/admin/main.php';

	if(isset($_POST['add-role-right-modal'])){
		$module_controller->insert_module(
			$_POST['add-modal-role-right-code'], $_POST['add-modal-role-right-name'],
			$_POST['add-modal-role-right-page']
		); 
	}

	if(isset($_POST['edit-role-right-modal'])){
		$module_controller->update_module(
			$_POST['edit-modal-role-right-code'], $_POST['edit-modal-role-right-name'],
			$_POST['edit-modal-role-right-page'], $_POST['edit-modal-hidden-module-code'],
			$_POST['edit-modal-hidden-module-name'], $_POST['edit-modal-hidden-module-page']
		);
	}

	if(isset($_POST['delete-module'])){
		$module_controller->delete_module($_POST['delete-modal-hidden-module-code']);
	}

	$text_content['all_modules']   = $module_controller->get_all_modules();
	$text_content['code']          = $text['module']['code'];
	$text_content['name']          = $text['module']['name'];
	$text_content['page']          = $text['module']['page'];
	$text_content['modal_message'] = $text['modal']['message'];
	$text_content['save']          = $text['common']['save'];
	$text_content['action_page']   = 'module.php'; //Decides the action page. It is not for view.

	echo $twig->render('admin/role_rights/module.html', $text_content);

?>