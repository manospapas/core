<?php
	
	require ($_SERVER['DOCUMENT_ROOT'] ."/app/models/roles_rights/rights_model.php");

	class RightsController extends RoleAccessController{

		public $rights_model;
		//This is the sum of Add, Edit, Delete, View, RowAcess. If you add more then this number should be increased.
		protected const NOF_RIGHTS = 5;  

		public function __construct() {
			parent::__construct(); 
			return $this->rights_model = new RightsModel();
		}

		public function show_all_role_rights($role) {
			return $this->rights_model->read_rights($role);
		}

		public function update_rights($role) {
			$modules = $this->show_all_role_rights($role);
			$i=-1;
			foreach($modules as $module){
				$i++;
				$set='';

				for($j=0; $j<self::NOF_RIGHTS; $j++){	
					try{
						if (isset($_POST['permissions'][0]) && $_POST['permissions'][0] == $module['role_right_id'].'-'.($j+1)) {
							$set .= $this->set_rights($j, 1);
							array_shift($_POST['permissions']);
						}
						else {
							$set .= $this->set_rights($j, 0);
						}
					}
					catch (Exception $e){
						$set .= $this->set_rights($j, 0);
					}
				}
				$set = $this->rights_model->remove_last_comma($set);

				//Specific cases that must be checked. Warning do not change the spaces or the order, since the string will be compare themselves!
				$full[] = "role_right_add = 1, role_right_edit = 1, role_right_delete = 1, role_right_view = 1, role_right_row_access = 0";
				$full[] = "role_right_add = 0, role_right_edit = 0, role_right_delete = 0, role_right_view = 0, role_right_row_access = 1";
				$full[] = "role_right_row_access = 1";

				if ($set === $full[0] || $set === $full[1]) {
					$set = "role_right_add = 1, role_right_edit = 1, role_right_delete = 1, role_right_view = 1, role_right_row_access = 1";
				}
				else if($set === $full[2]){
					$set = "role_right_add = 1, role_right_edit = 1, role_right_delete = 1, role_right_view = 1, role_right_row_access = 1";
				}

				$id = $module['role_right_id'];
				//echo $set; die;
				$this->rights_model->update_rights($set, $id);
			}
		}

		private function set_rights($j,$right) {
			$set = '';
			switch ($j+1) {
		    case "1":
		    	$set .= "role_right_add = $right, ";
		        break;
		    case "2":
		    	$set .= "role_right_edit = $right, ";
		        break;
		    case "3":
		    	$set .= "role_right_delete = $right, ";
		        break;
		    case "4":
		    	$set .= "role_right_view = $right, ";
		        break;
		    case "5":
		    	$set .= "role_right_row_access = $right, ";
		        break;
			}
			return $set;
		}	

	}

?>