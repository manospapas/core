<?php
	
	require ($_SERVER['DOCUMENT_ROOT'] ."/app/models/roles_rights/module_model.php");

	class ModuleController extends RoleAccessController {

		public $module_model;
		
		public function __construct() {
			parent::__construct();
			return $this->module_model = new ModuleModel(); 
		}

		public function get_all_modules() {
			return $this->module_model->get_all_modules();
		}
		
		public function insert_module($module_code, $module_name, $module_page) {
			$module_code      = strtoupper($module_code);
			$sucessful_action = $this->module_model->insert_module($module_code, $module_name, $module_page);

			$sucessful_action = $this->module_model->insert_module_in_role_rights($module_code);
			// if($sucessful_action->rowCount() > 0) {

			// 	if($sucessful_action == false) {
			// 		$sucessful_action = $this->module_model->delete_module($module_code);
			// 		if($sucessful_action == false) {
			// 			echo 'Something Went Completely Wrong! Contact us!'; 
			// 			die();
			// 		}
			// 	}

			// }
		}

		public function update_module($module_code, $module_name, $module_page, $current_module_code, $current_module_name, $current_module_page) {
			$module_code      = strtoupper($module_code);
			$sucessful_action = $this->module_model->update_module($module_code, $module_name, $module_page, $current_module_code);
			$sucessful_action = $this->module_model->update_role_right_module_code($module_code, $current_module_code);	
			// if($sucessful_action->rowCount() > 0) {
				

			// 	if($sucessful_action->rowCount() == 0) 
			// 		$this->module_model->update_module($current_module_code, $current_module_name, $current_module_page, $module_code);
			// }
			// else 
			// 	$this->module_model->update_module($current_module_code, $current_module_name, $current_module_page, $module_code);

		}


		public function delete_module($module_code) {
			$sucessful_action = $this->module_model->delete_module($module_code);
			$sucessful_action = $this->module_model->delete_modules_from_rights($module_code);
			// if($sucessful_action->rowCount() > 0){

			// }
				
			// else 
			// 	return false;
		}
	}
?>






















