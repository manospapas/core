<?php

	require ($_SERVER['DOCUMENT_ROOT'] ."/app/models/roles_rights/role_model.php");

	class RoleController extends RoleAccessController {

		public $role_model;

		public function __construct() {
			parent::__construct(); 
			return $this->role_model = new RoleModel();
		}

		public function show_roles($except_current_role) {
			return $this->role_model->show_roles($except_current_role);
		}

		public function insert_role($role_code, $role_name) {
			$this->role_model->insert_role(strtoupper($role_code), $role_name);			
			(new RightsModel())->insert_rights(strtoupper($role_code));
		}

		public function update_role($role_code, $role_name, $current_role_code) {
			$this->role_model->update_role(strtoupper($role_code), $role_name, $current_role_code);
			(new RightsModel())->update_role_in_role_rights(strtoupper($role_code), $current_role_code);
		}

		public function delete_role($role_code) {
			$this->role_model->delete_role($role_code);
			(new RightsModel())->delete_rights($role_code);
		}

		public function get_all_roles() {
			return $this->role_model->get_all_roles();
		}

	}
?>