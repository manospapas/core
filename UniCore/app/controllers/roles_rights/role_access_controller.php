<?php
	
	require ($_SERVER['DOCUMENT_ROOT'] ."/app/models/roles_rights/role_access_model.php");

	class RoleAccessController {

		public $role_access_model;
		public $CRUD;

		public function __construct() {
			$this->set_crud_rights();
			return $this->role_access_model = new RoleAccessModel(); 
		}

		private function set_crud_rights() {
			$this->CRUD = (isset($_SESSION['CRUD'])) ? $_SESSION['CRUD'] : [];
		}

		public function get_crud_rights() {
			return $this->CRUD;
		}

		public function has_access($user, $file) {

			if(!isset($user->username) || empty($user->username) || !isset($user->email) || empty($user->email) || !isset($user->role) || empty($user->role)) {
				//TODO logout?
				header("Location: /app/views/registration/login.php");
				return true; //We return true so that the redirect will take place.
			}
			$has_access = $this->role_access_model->allows_access($user->role, $file);

			if($has_access['role_right_view'] || true) {
				$this->CRUD   = []; 
				$this->CRUD[] = ($has_access['role_right_view'])   ? 'view'   : 'no-view';
				$this->CRUD[] = ($has_access['role_right_add'])    ? 'create' : 'no-create';
				$this->CRUD[] = ($has_access['role_right_edit'])   ? 'edit'   : 'no-edit';
				$this->CRUD[] = ($has_access['role_right_delete']) ? 'delete' : 'no-delete';
				$_SESSION['CRUD'] = $this->CRUD; 
				return true;				
			}
			else return false;
		}

		public function get_role_menu($role){
			$results = $this->role_access_model->get_menu_items($role);
			$menu = [];

			foreach ($results as $result) {	
				$included_roles = explode(" ", $result['menu_item_visibility']);
				if(in_array($role, $included_roles)) {
			    	$menu[] = $result;	
				}
			}

			return $menu;
		}

	}

?>