<?php
	
	require ($_SERVER['DOCUMENT_ROOT'] ."/app/models/users/user_model.php");

	class UserController {

		protected $user_model;

		public function __construct() { 
			return $this->user_model = new UserModel(); 
		}

		protected function get_user_model() { return $this->user_model; }

		public function get_all_users() { return $this->get_user_model()->get_all_users(); }

		public function insert_new_user($username, $firstname, $lastname, $email, $password, $role) {
			$this->get_user_model()->insert_new_user($username, $firstname, $lastname, $email, $password, $role); 
		}

		public function delete_user($id) {
			$this->get_user_model()->delete_user($id); 
		}

	} // End of Class

?>