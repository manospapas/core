<?php
	
	require ($_SERVER['DOCUMENT_ROOT'] ."/app/controllers/users/user_controller.php");

	class CurrentUserController extends UserController {

		public $email, $username, $role;
		private $message;

		public function __construct() {
			parent::__construct(); 
			$this->email             = (isset($_SESSION['current_user_email']))? $_SESSION['current_user_email'] : '';
			$this->username          = (isset($_SESSION['current_user_name']))? $_SESSION['current_user_name'] : '';
			$this->role              = (isset($_SESSION['current_user_role']))? $_SESSION['current_user_role'] : '';
		}		

		public function get_message() {	return $this->message; }

		public function set_message($message) {	$this->message = $message; }

		public function set_current_user_data($email, $username, $role) {
			$_SESSION['current_user_email'] = $this->email    = $email;
			$_SESSION['current_user_name']  = $this->username = $username;
			$_SESSION['current_user_role']  = $this->role     = $role;
		}

		public function login($email, $password) {
						
			if(empty($email) || empty($password)) return  $this->set_message($GLOBALS['text']['login']['empty']);

		    if (($result = $this->get_user_model()->email_exist($email))->rowCount() > 0) { //User Exists.
		      	$current_user = $this->get_user_model()->fetch_result($result);
		    	if(password_verify($password, $current_user['user_password'])) { //Successful Login.
		    		$this->set_current_user_data($current_user['user_email'], $current_user['user_name'], $current_user['user_role']);
			        $this->set_message(''); //Empty the message. 
			       //TODO what is main for each one?
			       header("location: /app/views/admin/dashboard.php");
		    	}
		    	else $this->set_message($GLOBALS['text']['login']['exist']);
		    }
		    else $this->set_message($GLOBALS['text']['login']['exist']);
		}

		public function logout() {
			$current_locale = $_SESSION['language'];
			unset($this->user_model);
			unset($_SESSION);
			session_destroy(); 
			$_SESSION['language'] = $current_locale;
		}

		public function forgot_password($email) {
			if(!empty($email)) {

				if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) { 

				    if ( ($result = $this->get_user_model()->email_exist($email))->rowCount() > 0) {
				        $current_user = $this->get_user_model()->fetch_result($result); 				        
				        $email = $current_user['user_email'];
				        $hash_code = $current_user['user_hash_code'];
				        $this->set_message($GLOBALS['text']['forgot']['check_email']);
				        // Send registration confirmation link (reset.php)
				        $to           = $email;
				        $subject      = $GLOBALS['text']['forgot']['subject'];
				        $locale       = (isset($_GET['language'])) ? '&language='. $_GET['language'] : '';
				        $message_body = $GLOBALS['text']['forgot']['body'].'?email='.$email.'&hash='.$hash_code . $locale;  
				        mail($to, $subject, $message_body);	

				        $this->set_message($GLOBALS['text']['forgot']['email_send']);			    	
				    }	        
				    else $this->set_message($GLOBALS['text']['forgot']['exist']);
				}
				
			}
			else $this->set_message($GLOBALS['text']['forgot']['provide_email']);
		}

		public function reset_password($email, $hash, $new_password, $new_password_repeat) {
			if(!empty($email) && !empty($hash) ) {

				if ($this->get_user_model()->valid_arguments($email, $hash)->rowCount() > 0) {

					if ($_SERVER['REQUEST_METHOD'] == 'POST') { 

					    if ($new_password == $new_password_repeat) { 
					    	$new_password = $this->get_user_model()->encrypt_password($new_password);
					        $hash_code    = $this->get_user_model()->generate_hash();
					        $result       = $this->get_user_model()->reset_password($new_password, $hash_code, $email);

					        if ($result->rowCount() > 0) $this->set_message($GLOBALS['text']['reset']['success']);
					    }
					    else $this->set_message($GLOBALS['text']['reset']['match']);   					    
					}
				}
				else $this->set_message($GLOBALS['text']['reset']['credentials']);
			}
			else $this->set_message($GLOBALS['text']['reset']['not_found']);
		}

	} // End of Class

?>