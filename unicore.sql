-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 11, 2019 at 11:16 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `unicore`
--

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `menu_name` (`menu_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`menu_id`, `menu_name`) VALUES
(1, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE IF NOT EXISTS `menu_items` (
  `menu_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_item_parent` tinyint(4) NOT NULL,
  `menu_item_position` varchar(10) CHARACTER SET utf8 NOT NULL,
  `menu_item_visibility` text CHARACTER SET utf8 NOT NULL,
  `menu_item_code` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`menu_item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`menu_item_id`, `menu_item_parent`, `menu_item_position`, `menu_item_visibility`, `menu_item_code`) VALUES
(1, 0, '1', 'GOD ADMIN ', 'DASHBOARD'),
(2, 0, '2', 'GOD ADMIN', 'EXAMS'),
(5, 0, '3', 'GOD ADMIN', 'CENTERS'),
(6, 0, '4', 'GOD SUPER_ADMIN', 'CANDIDATE'),
(7, 6, '41', 'GOD SUPER_ADMIN', 'NEW'),
(8, 6, '42', 'ADMIN SUPER_ADMIN', 'OLD'),
(9, 0, '5', 'GOD PTE', 'EXAM_CENTERS'),
(10, 0, '6', 'GOD PTE', 'MENU_LEVELS'),
(11, 0, '7', 'GOD SUPER_ADMIN ADMIN', 'ROLES'),
(12, 0, '8', 'GOD ADMIN', 'USERS');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `module_code` varchar(50) NOT NULL,
  `module_name` varchar(50) NOT NULL,
  `module_page` varchar(150) NOT NULL,
  PRIMARY KEY (`module_code`,`module_name`),
  UNIQUE KEY `mod_modulecode` (`module_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_code`, `module_name`, `module_page`) VALUES
('CANDIDATE', 'Candidate', 'candidate.php'),
('DASHBOARD', 'Dashboard', 'dashboard.php'),
('EXAM', 'Exam', 'exam.php'),
('MODULE', 'Module', 'module.php'),
('ROLE', 'Role', 'role.php'),
('ROLE_RIGHTS', 'Role Rights', 'role_rights.php');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_code` varchar(50) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  PRIMARY KEY (`role_code`),
  UNIQUE KEY `role_code` (`role_code`),
  UNIQUE KEY `role_name` (`role_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_code`, `role_name`) VALUES
('GOD', 'God'),
('ADMIN', 'Admin'),
('LOWER_ADMIN', 'Lower Admin'),
('VIEWER', 'Viewer'),
('EDITOR', 'Editor'),
('SALESMAN', 'Sales Man'),
('LOCAL_SALESMAN', 'Local Sales Man'),
('CONTROLLER', 'Controller'),
('CENTERS', 'Center');

-- --------------------------------------------------------

--
-- Table structure for table `role_rights`
--

DROP TABLE IF EXISTS `role_rights`;
CREATE TABLE IF NOT EXISTS `role_rights` (
  `role_right_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_right_role_code` varchar(50) NOT NULL,
  `role_right_module_code` varchar(50) NOT NULL,
  `role_right_view` tinyint(1) NOT NULL DEFAULT '0',
  `role_right_add` tinyint(1) NOT NULL DEFAULT '0',
  `role_right_edit` tinyint(1) NOT NULL DEFAULT '0',
  `role_right_delete` tinyint(1) NOT NULL DEFAULT '0',
  `role_right_row_access` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_right_id`),
  KEY `rr_rolecode` (`role_right_role_code`),
  KEY `rr_modulecode` (`role_right_module_code`)
) ENGINE=MyISAM AUTO_INCREMENT=392 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_rights`
--

INSERT INTO `role_rights` (`role_right_id`, `role_right_role_code`, `role_right_module_code`, `role_right_view`, `role_right_add`, `role_right_edit`, `role_right_delete`, `role_right_row_access`) VALUES
(190, 'ADMIN', 'DASHBOARD', 1, 1, 1, 1, 1),
(191, 'ADMIN', 'ROLE_RIGHTS', 1, 1, 1, 1, 1),
(192, 'ADMIN', 'ROLE', 1, 1, 1, 1, 1),
(193, 'ADMIN', 'MODULE', 1, 1, 1, 1, 1),
(252, 'GOD', 'ROLE_RIGHTS', 1, 1, 1, 1, 1),
(251, 'GOD', 'ROLE', 1, 1, 1, 1, 1),
(250, 'GOD', 'MODULE', 1, 1, 1, 1, 1),
(249, 'GOD', 'EXAM', 1, 1, 1, 1, 1),
(248, 'GOD', 'DASHBOARD', 1, 1, 1, 1, 1),
(388, 'VIEWER', 'CANDIDATE', 0, 0, 0, 0, 0),
(389, 'ADMIN', 'CANDIDATE', 0, 0, 0, 0, 0),
(390, 'ADMIN', 'CANDIDATE', 0, 0, 0, 0, 0),
(391, 'ADMIN', 'CANDIDATE', 0, 0, 0, 0, 0),
(387, 'SALESMAN', 'CANDIDATE', 0, 0, 0, 0, 0),
(384, 'LOWER_ADMIN', 'CANDIDATE', 0, 0, 0, 0, 0),
(383, 'LOCAL_SALESMAN', 'CANDIDATE', 0, 0, 0, 0, 0),
(382, 'GOD', 'CANDIDATE', 1, 1, 1, 1, 1),
(379, 'EDITOR', 'CANDIDATE', 0, 0, 0, 0, 0),
(378, 'CONTROLLER', 'CANDIDATE', 0, 0, 0, 0, 0),
(377, 'CENTERS', 'CANDIDATE', 1, 0, 0, 0, 0),
(376, 'ADMIN', 'CANDIDATE', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_first_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_last_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_role` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_hash_code` varchar(500) CHARACTER SET utf8 NOT NULL,
  `user_password` varchar(500) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_first_name`, `user_last_name`, `user_email`, `user_role`, `user_hash_code`, `user_password`) VALUES
(1, 'Manos', 'Manos', 'Papagiannidis', 'e.papagiannidis@gmail.com', 'ADMIN', '$2y$12$7cSf9ZYC7GX1t3em71BlC.puiAYEcQVP86tx85I1DufYX/o.Zs/K.', '$2y$12$o0YCQU.wlEKdLQ4xMaWY0.vU9d238CATtZ9ReSjDdtVqOCs4X7gGe'),
(13, 'user@mail.com', 'user1', 'user1', 'user@mail.com', 'None', '$2y$12$x8PwpYflqTxBOe7yNnmfFO0Pd96WthbLlfDtJ.j10TiwwcsTMbTHS', '$2y$12$8h8YqWZtfQZECpwuB6aQXuRKRIjLKa5ipY2/xjZ6JMXwB23GbnMES'),
(12, 'user@mail.com', 'user1', 'user1', 'user@mail.com', 'GOD', '$2y$12$v7pxhaENiyzG70sJgjBU7O2X9pCFs0vBcDuOj4gGkd13yIhJ4HZ9m', '$2y$12$z6NtgN6qYLPjhXvgXuQwZeCuLHp7JyhPK0eWd/QDBjF7c8IYVTLze');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
